<?php


namespace Aboutgoods\KonnectBundle\Controller;

use GuzzleHttp\Client;
use Aboutgoods\KonnectBundle\KonnectClient;
use Symfony\Component\HttpFoundation\Response;
use Aboutgoods\KonnectBundle\Services\KonnectApi;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;


class ServiceController extends AbstractController
{
    /**
     * Get a list of all the services that exists to create the menu
     * @Route("/services/menu", name="services.menu")
     *
     * @param KonnectClient $konnectClient
     *
     * @return Response
     */
    public function getServicesListForMenu(KonnectClient $konnectClient)
    {
        $client = $konnectClient->getGuzzleClient();
        $response = $client->get("api/services/menu");
        return new Response($response->getBody()->getContents(), Response::HTTP_OK, ['Content-Type' => 'application/json']);
    }
}