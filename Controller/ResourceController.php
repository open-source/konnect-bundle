<?php
/**
 * Created by PhpStorm.
 * User: albancrepel
 * Date: 2019-12-09
 * Time: 14:51
 */

namespace Aboutgoods\KonnectBundle\Controller;

use Aboutgoods\KonnectBundle\ImgProxyClient;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ResourceController extends AbstractController
{
    /**
     * Get a resource through our imgproxy server
     * @Route("/resource")
     * @return Response
     */
    public function getResource(ImgProxyClient $imgProxyClient, Request $request)
    {
        // we encode the url in base64 on the front to avoid problems when passing URLS to the backend
        // so we have to decode it
        $url = base64_decode($request->query->get("url"));
        $resize = $request->query->get("resize", "fit");
        $width = $request->query->get("width", "0");
        $height = $request->query->get("height", "0");
        $gravity = $request->query->get("gravity", "ce");
        $enlarge = $request->query->get("enlarge", 1);
        //If we don't provide the defaultExtension parameter, we transform the image to png by default
        $extension = $request->query->get("useDefaultExtension", false) ? null : "png";

        $path = $imgProxyClient->getResourceUrl($url, $resize, $width, $height, $gravity, $enlarge, $extension);
        try {
            $content = file_get_contents($path);
            return new Response($content, 200, ["Content-Type" => "application/octet-stream"]);
        } catch (\Throwable $throwable) {
            return new Response("Resource not found", 404);
        }
    }
}