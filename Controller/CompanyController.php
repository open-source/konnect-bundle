<?php

namespace Aboutgoods\KonnectBundle\Controller;

use GuzzleHttp\Client;
use GuzzleHttp\ClientInterface;
use Aboutgoods\KonnectBundle\KonnectClient;
use Aboutgoods\KonnectBundle\Services\KonnectApi;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;


class CompanyController extends AbstractController
{
    /**
     * Send a request to Kweeri Konnect to reset the company hydra token
     * @Route("/company/reset_secret")
     *
     * @param KonnectClient $konnectClient
     *
     * @return Response
     */
    public function resetCompanySecret(KonnectClient $konnectClient)
    {
        if (!in_array("ROLE_OWNER", $this->getUser()->getRoles())) {
            return new Response("Only owner is allowed to reset the secret", Response::HTTP_FORBIDDEN);
        }
        $client = $konnectClient->getGuzzleClient();
        $companyId = $this->getUser()->getCompanyId();
        
        $response = $client->get("api/company/$companyId/reset_secret");
        return new Response($response->getBody()->getContents(), Response::HTTP_OK, ['Content-Type' => 'application/json']);
    }

    /**
     * Get current user company from antar
     * @Route("/company")
     * @param KonnectClient $konnectClient
     * @return Response
     */
    public function getCompanyFromAntar(KonnectClient $konnectClient) {
        $companyUid = $this->getUser()->getCompanyId();
        $client = $konnectClient->getGuzzleClient();
        $response = $client->get("api/company/$companyUid");
        return new Response($response->getBody()->getContents(), Response::HTTP_OK, ['Content-Type' => 'application/json']);
    }
}