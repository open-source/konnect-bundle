<?php

namespace Aboutgoods\KonnectBundle\Controller;

use Aboutgoods\KonnectBundle\KonnectClient;
use Aboutgoods\KonnectBundle\Services\KonnectApi;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use GuzzleHttp\Client;

/**
 * Created by PhpStorm.
 * User: lucas
 * Date: 12/11/2018
 * Time: 09:31
 */

class UserController extends AbstractController
{
    /**
     * @Route("/logout", name="konnect.logout")
     * This route is a shortcut to call the route auth0_logout of the HWIOauth Bundle
     * @return RedirectResponse
     */
    public function logout()
    {
        return $this->redirectToRoute('auth0_logout');
    }
}