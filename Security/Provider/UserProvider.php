<?php
namespace Aboutgoods\KonnectBundle\Security\Provider;


use Aboutgoods\KonnectBundle\Model\User;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use HWI\Bundle\OAuthBundle\OAuth\Response\UserResponseInterface;
use Symfony\Component\Security\Core\Exception\UnsupportedUserException;
use HWI\Bundle\OAuthBundle\Security\Core\User\OAuthAwareUserProviderInterface;
use function Couchbase\defaultDecoder;

//Todo: Maybe put this into a specific library
class UserProvider implements UserProviderInterface, OAuthAwareUserProviderInterface
{
    const AUTH0_USER_DATA_KEY_PICTURE = 'picture';
    // In Auth0 the `name` key is equal to the email of the user
    const AUTH0_USER_DATA_KEY_EMAIL= 'name';
    const AUTH0_USER_DATA_KEY_NICKNAME = 'nickname';
    // `sub` is equal to the user id in Auth0
    const AUTH0_USER_DATA_KEY_ID = 'sub';

    const AUTH0_USER_APP_METADATA_KEY = 'http://konnect/app_metadata';

    const AUTH0_USER_APP_METADATA_KEY_COMPANYID = 'company_id';
    const AUTH0_USER_APP_METADATA_KEY_STRIPE = 'stripe_customer_id';
    const AUTH0_USER_APP_METADATA_KEY_ROLES = 'roles';
    const AUTH0_USER_APP_METADATA_KEY_SUBSCRIPTIONS = 'subscriptions';

    const AUTH0_USER_METADATA_KEY = 'http://konnect/user_metadata';

    const AUTH0_USER_METADATA_KEY_GIVEN_NAME = 'given_name';

    /**
     * {@inheritdoc}
     */
    public function loadUserByUsername($username)
    {
        return new User($username);
    }

    /**
     * This method is called by HWIOAuthBundle
     * Loading user from OAuthUser received from Auth0 (in our case from Auth0 but it could be from any Identity Provider using OAuth)
     * @see https://github.com/hwi/HWIOAuthBundle
     */
    public function loadUserByOAuthUserResponse(UserResponseInterface $response)
    {
        $auth0ParsedObject = $response->getData();
        $user = new User($auth0ParsedObject[self::AUTH0_USER_DATA_KEY_ID]);
        $user->setEmail($auth0ParsedObject[self::AUTH0_USER_DATA_KEY_EMAIL]);
        $user->setUsername($auth0ParsedObject[self::AUTH0_USER_DATA_KEY_NICKNAME]);
        $user->setGivenName($auth0ParsedObject[self::AUTH0_USER_METADATA_KEY_GIVEN_NAME] ?? $auth0ParsedObject[self::AUTH0_USER_DATA_KEY_NICKNAME]);
        $user->setPicture($auth0ParsedObject[self::AUTH0_USER_DATA_KEY_PICTURE]);
        $user->setCompanyId($auth0ParsedObject[self::AUTH0_USER_APP_METADATA_KEY][self::AUTH0_USER_APP_METADATA_KEY_COMPANYID]);
        $user->setSubscriptions($auth0ParsedObject[self::AUTH0_USER_APP_METADATA_KEY][self::AUTH0_USER_APP_METADATA_KEY_SUBSCRIPTIONS]);
        $user->setStripeCustomerId($auth0ParsedObject[self::AUTH0_USER_APP_METADATA_KEY][self::AUTH0_USER_APP_METADATA_KEY_STRIPE]);
        $user->addRoles($auth0ParsedObject[self::AUTH0_USER_APP_METADATA_KEY][self::AUTH0_USER_APP_METADATA_KEY_ROLES]);
        return $user;
    }

    /**
     * {@inheritdoc}
     */
    public function refreshUser(UserInterface $user)
    {
        if (!$this->supportsClass(get_class($user))) {
            throw new UnsupportedUserException(sprintf('Unsupported user class "%s"', get_class($user)));
        }

        return $user;
    }

    /**
     * {@inheritdoc}
     */
    public function supportsClass($class)
    {
        return User::class === $class;
    }
}