<?php


namespace Aboutgoods\KonnectBundle\Security;

use Symfony\Component\OptionsResolver\Options;
use Symfony\Component\OptionsResolver\OptionsResolver;

use HWI\Bundle\OAuthBundle\OAuth\ResourceOwner\GenericOAuth2ResourceOwner;

//Todo: Maybe put this into a specific library
class Auth0ResourceOwner extends GenericOAuth2ResourceOwner
{

    const AUTH0_USER_DATA_KEY_PICTURE = 'picture';
    const AUTH0_USER_DATA_KEY_EMAIL = 'email';
    const AUTH0_USER_DATA_KEY_NAME = 'name';
    const AUTH0_USER_DATA_KEY_NICKNAME = 'nickname';
    const AUTH0_USER_DATA_KEY_USERID = 'user_id';

    const AUTH0_USER_DATA_KEY_USERID_KEY = 'identifier';
    const AUTH0_USER_DATA_KEY_NICKNAME_KEY = 'nickname';
    const AUTH0_USER_DATA_KEY_NAME_KEY = 'realname';
    const AUTH0_USER_DATA_KEY_EMAIL_KEY = 'email';
    const AUTH0_USER_DATA_KEY_PICTURE_KEY = 'profilepicture';

    const AUTH0_URI_AUTHORIZE = '/authorize';
    const AUTH0_URI_ACCESS_TOKEN = '/oauth/token';
    const AUTH0_URI_INFO = '/userinfo';

    const AUTH0_URI_AUTHORIZE_KEY = 'authorization_url';
    const AUTH0_URI_ACCESS_TOKEN_KEY = 'access_token_url';
    const AUTH0_URI_INFO_KEY = 'infos_url';
    const AUTH0_URI_AUDIENCE_KEY = 'audience';

    /**
     * {@inheritdoc}
     */
    protected $paths = [
        self::AUTH0_USER_DATA_KEY_USERID_KEY => self::AUTH0_USER_DATA_KEY_USERID,
        self::AUTH0_USER_DATA_KEY_NICKNAME_KEY => self::AUTH0_USER_DATA_KEY_NICKNAME,
        self::AUTH0_USER_DATA_KEY_NAME_KEY => self::AUTH0_USER_DATA_KEY_NAME,
        self::AUTH0_USER_DATA_KEY_EMAIL_KEY => self::AUTH0_USER_DATA_KEY_EMAIL,
        self::AUTH0_USER_DATA_KEY_PICTURE_KEY => self::AUTH0_USER_DATA_KEY_PICTURE,
    ];

    /**
     * {@inheritdoc}
     */
    public function getAuthorizationUrl($redirectUri, array $extraParameters = [])
    {
        return parent::getAuthorizationUrl($redirectUri, array_merge([
                                                                         'audience' => $this->options['audience'],
                                                                     ], $extraParameters));
    }

    /**
     * {@inheritdoc}
     */
    protected function configureOptions(OptionsResolver $resolver)
    {
        parent::configureOptions($resolver);

        $resolver->setDefaults([
                                   self::AUTH0_URI_AUTHORIZE_KEY => '{base_url}' . self::AUTH0_URI_AUTHORIZE,
                                   self::AUTH0_URI_ACCESS_TOKEN_KEY => '{base_url}' . self::AUTH0_URI_ACCESS_TOKEN,
                                   self::AUTH0_URI_INFO_KEY => '{base_url}' . self::AUTH0_URI_INFO,
                                   self::AUTH0_URI_AUDIENCE_KEY => '{base_url}' . self::AUTH0_URI_INFO,
                               ]);

        $resolver->setRequired([
                                   'base_url',
                               ]);

        $normalizer = static function (Options $options, $value) {
            return str_replace('{base_url}', $options['base_url'], $value);
        };

        $resolver->setNormalizer(self::AUTH0_URI_AUTHORIZE_KEY, $normalizer);
        $resolver->setNormalizer(self::AUTH0_URI_ACCESS_TOKEN_KEY, $normalizer);
        $resolver->setNormalizer(self::AUTH0_URI_INFO_KEY, $normalizer);
        $resolver->setNormalizer(self::AUTH0_URI_AUDIENCE_KEY, $normalizer);
    }
}