<?php

namespace Aboutgoods\KonnectBundle\Security;


use AboutGoods\Antar\Models\Company;
use AboutGoods\Antar\Models\Feature;
use Aboutgoods\KonnectBundle\AntarClient;
use Aboutgoods\KonnectBundle\Model\User;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;

class AntarVoter extends Voter
{
    private $featuresAvailable = [
        Feature::FEATURE_CSV_EXPORT,
        Feature::FEATURE_QUALITY_CHECK,
        Feature::FEATURE_PROMOTIONS,
        Feature::FEATURE_RECEIPT,
        Feature::FEATURE_SHERLOCK,
    ];
    private $companyLevelAvailable = [
        Company::SECURITY_LEVEL_ADMIN,
        Company::SECURITY_LEVEL_CLIENT,
        Company::SECURITY_LEVEL_READ_ONLY,
    ];

    /**
     * @var AntarClient
     */
    private $antarClient;

    /**
     * AntarVoter constructor.
     * @param AntarClient $antarClient
     */
    public function __construct(AntarClient $antarClient)
    {
        $this->antarClient = $antarClient;
    }

    protected function supports($attribute, $subject)
    {
        // allow COMPANY_LEVEL and FEATURE_NAME and FEATURE_NAME.QUOTA with this arraymap
        return in_array($attribute, $this->companyLevelAvailable)
            || in_array($attribute, $this->featuresAvailable)
            || in_array($attribute, array_map(function ($tag) {
                return $tag . ".QUOTA";
            }, $this->featuresAvailable));
    }

    /*
    * This function is called (if the support method is supports it) at any isGranted or denyUnlessGranted call. 
    * It will check if the user has the feature in Antar passed as parameter. 
    * If the feature name is suffixed with .QUOTA, it will also check if there is remaining quota. (aka: not 0 quota)
    */
    protected function voteOnAttribute($attribute, $subject, TokenInterface $token)
    {

        $user = $token->getUser();
        if (!($user instanceof User)) {
            return false;
        }
        if (in_array($attribute, $this->companyLevelAvailable)) {
            return $this->antarClient->hasLevel($user->getCompanyId(), $attribute);
        }
        $keys = explode(".", $attribute);
        if (isset($keys[1]) && $keys[1] === "QUOTA") {
            return $this->antarClient->isGrantedAndHasEnoughQuota($user->getCompanyId(), $keys[0]);
        }
        //Explode without dot is always set on key 0 so I could have used $attribute as well.
        return $this->antarClient->getFeature($user->getCompanyId(), $keys[0]) !== null;
    }

}