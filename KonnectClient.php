<?php
/**
 * Created by PhpStorm.
 * User: henri
 * Date: 7/16/18
 * Time: 9:48 AM
 */
namespace Aboutgoods\KonnectBundle;

use GuzzleHttp\Client;
use GuzzleHttp\ClientInterface;
use Aboutgoods\KonnectBundle\Event\LoginEvent;
use Aboutgoods\KonnectBundle\Event\LogoutEvent;
use net\aboutgoods\konnect\Konnect;
use net\aboutgoods\konnect\model\response\Authorization;
use net\aboutgoods\konnect\model\response\User;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

/**
 * Class KonnectClient
 * @package Aboutgoods\KonnectBundle
 * This class allow symfony to get the config from the `konnect.yml`
 */
class KonnectClient
{
    private $configuration;

    public function __construct(Configuration $configuration)
    {
        $this->configuration = $configuration->getConfiguration(Configuration::AUTHENTICATION);
    }

    public function getGuzzleClient()
    {
        return new Client([
            "base_uri" => $this->configuration['url'],
            "auth" => [
                $this->configuration['token'],
                $this->configuration['secret']
            ]
        ]);
    }
}