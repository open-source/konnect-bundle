<?php
/**
 * Created by PhpStorm.
 * User: albancrepel
 * Date: 2019-12-09
 * Time: 15:06
 */

namespace Aboutgoods\KonnectBundle;


class ImgProxyClient
{
    private $configuration;

    public function __construct(Configuration $configuration)
    {
        $this->configuration = $configuration->getConfiguration(Configuration::IMGPROXY);
    }

    public function getResourceUrl($url, $resize, $width, $height, $gravity, $enlarge, $extension){
        $encodedUrl = rtrim(strtr(base64_encode($url), '+/', '-_'), '=');

        $key = $this->configuration["key"];
        $salt = $this->configuration["salt"];
        $imgProxyUrl = $this->addTrailingSlashIfNotExists($this->configuration["url"]);

        $keyBin = pack("H*" , $key);
        if(empty($keyBin)) {
            die('Key expected to be hex-encoded string');
        }
        $saltBin = pack("H*" , $salt);
        if(empty($saltBin)) {
            die('Salt expected to be hex-encoded string');
        }

        $extensionSuffix = $extension ? ".{$extension}" : "";

        $path = "/rs:{$resize}:{$width}:{$height}/g:{$gravity}/el:{$enlarge}/{$encodedUrl}{$extensionSuffix}";
        $signature = rtrim(strtr(base64_encode(hash_hmac('sha256', $saltBin.$path, $keyBin, true)), '+/', '-_'), '=');

        return $imgProxyUrl . $signature . $path;
    }

    private function addTrailingSlashIfNotExists($url) {
        return substr($url, -1) === "/" ?
            $url :
            "$url/";
    }
}