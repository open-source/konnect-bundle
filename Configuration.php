<?php
/**
 * Created by PhpStorm.
 * User: albancrepel
 * Date: 2019-12-12
 * Time: 10:00
 */

namespace Aboutgoods\KonnectBundle;


class Configuration
{
    const AUTHENTICATION = "authentication";
    const IMGPROXY = "imgproxy";
    const ANTAR = "antar";

    private $configuration = [
        self::AUTHENTICATION => [
            "token"  => "token",
            "secret" => "secret",
            "url"    => null,
        ],
        self::IMGPROXY       => [
            "key"  => "key",
            "salt" => "salt",
            "url"  => null,
        ],
        self::ANTAR          => [
            "antarUrl" => "https://receipt.kweeri.io/",
            "hydraUrl" => "https://security.agkit.io/",
            "token"    => "aboutgoods",
            "secret"   => null,
        ],
    ];

    public function getConfiguration($name)
    {
        return $this->configuration[$name];
    }

    /**
     * This method is called in the KonnectExtension.php
     * @param $config
     */
    public function setConfiguration($config)
    {
        // Replace missing values with the default configuration on top of the class
        foreach ($this->configuration as $key => $value) {
            if (is_array($value)) {
                foreach ($value as $subKey => $subValue) {
                    if (isset($config[$key][$subKey])) {
                        $this->configuration[$key][$subKey] = $config[$key][$subKey];
                    }
                }
            } else {
                if (isset($config[$key])) {
                    $this->configuration[$key] = $config[$key];
                }
            }
        }
    }
}