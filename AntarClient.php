<?php


namespace Aboutgoods\KonnectBundle;


use AboutGoods\Antar\Client;
use AboutGoods\Antar\Models\Company;
use AboutGoods\Antar\Models\Feature;

class AntarClient
{
    /**
     * @var Client
     */
    private $antar;

    public function __construct(Configuration $configuration)
    {
        $configurationList = $configuration->getConfiguration(Configuration::ANTAR);
        $this->antar = new Client($configurationList["antarUrl"], $configurationList["hydraUrl"], $configurationList["token"], $configurationList["secret"]);
    }

    public function __invoke($impersonate = null): Client
    {
        if (null !== $impersonate) {
            return $this->antar->impersonate($impersonate);
        }
        return $this->antar;

    }

    public function hasLevel($companyUid, $level, $strict = false): bool
    {
        $company = $this()->companies->get($companyUid);
        if ($strict) {
            return $company->getLevel() === $level;
        }
        $rolesHierarchy = [
            Company::SECURITY_LEVEL_ADMIN     => 100,
            Company::SECURITY_LEVEL_CLIENT    => 10,
            Company::SECURITY_LEVEL_READ_ONLY => 0,
        ];
        return $rolesHierarchy[$company->getLevel()] >= $rolesHierarchy[$level];
    }

    public function getFeature($companyUid, $featureName): ?Feature
    {
        //$this() for the invoke method.
        $company = $this()->companies->get($companyUid);
        if ($company->getLevel() == Company::SECURITY_LEVEL_ADMIN) {
            return new Feature($featureName, -1);
        }
        foreach ($company->getFeatures() as $feature) {
            if ($feature->getName() == $featureName) {
                return $feature;
            }
        }
        return null;
    }

    public function isGrantedAndHasEnoughQuota($companyUid, $featureName)
    {
        $feature = $this->getFeature($companyUid, $featureName);
        if (null !== $feature) {
            return $feature->getQuota() !== 0;
        }
        return false;
    }
}