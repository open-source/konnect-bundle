<?php
namespace Aboutgoods\KonnectBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;
use Aboutgoods\KonnectBundle\Configuration as Config;

/**
 * This is the class that validates and merges configuration from your app/config files.
 *
 * To learn more see {@link http://symfony.com/doc/current/cookbook/bundles/configuration.html}
 */
class Configuration implements ConfigurationInterface
{
    /**
     * {@inheritdoc}
     */
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder();
        $rootNode = $treeBuilder->root('konnect');
        $rootNode
            ->children()
                ->arrayNode(Config::AUTHENTICATION)
                    ->children()
                        ->scalarNode('token')->defaultValue("changeMe")->end()
                        ->scalarNode('secret')->defaultValue("changeMe")->end()
                        ->scalarNode('url')->defaultValue(null)->end()
                    ->end()
                ->end()
                ->arrayNode(Config::ANTAR)
                    ->children()
                        ->scalarNode('antarUrl')->defaultValue(null)->end()
                        ->scalarNode('hydraUrl')->defaultValue(null)->end()
                        ->scalarNode('token')->end()
                        ->scalarNode('secret')->end()
                    ->end()
                ->end()
                ->arrayNode(Config::IMGPROXY)
                    ->children()
                        ->scalarNode('key')->defaultValue("changeMe")->end()
                        ->scalarNode('salt')->defaultValue("changeMe")->end()
                        ->scalarNode('url')->defaultValue(null)->end()
                    ->end()
                ->end()
                ->end()
            ->end()
        ;
        return $treeBuilder;
    }
}
