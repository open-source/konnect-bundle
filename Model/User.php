<?php


namespace Aboutgoods\KonnectBundle\Model;


use App\Services\Auth0\ManagementAPI;
use Exception;
use JsonSerializable;
use Symfony\Component\Security\Core\User\UserInterface;

class User implements UserInterface, JsonSerializable
{
    protected $id;
    protected $email;
    protected $givenName;
    protected $picture;
    protected $companyId;
    protected $subscriptions;
    protected $stripeCustomerId;
    protected $roles;

    public function __construct($id) {
        $this->id = $id;
        $this->roles = ["ROLE_USER"];
    }

    /**
     * @return mixed
     */
    public function getGivenName()
    {
        return $this->givenName;
    }

    /**
     * @param mixed $givenName
     *
     * @return User
     */
    public function setGivenName($givenName): User
    {
        $this->givenName = $givenName;
        return $this;
    }

    /**
     * @param mixed $email
     *
     * @return User
     */
    public function setEmail($email): User
    {
        $this->email = $email;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }


    /**
     * @return mixed
     */
    public function getPicture()
    {
        return $this->picture;
    }

    /**
     * @param mixed $picture
     *
     * @return User
     */
    public function setPicture($picture): User
    {
        $this->picture = $picture;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getCompanyId()
    {
        return $this->companyId;
    }

    /**
     * @param $companyId
     *
     * @return User
     */
    public function setCompanyId($companyId): User
    {
        $this->companyId = $companyId;
        return $this;
    }


    /**
     * @return mixed
     */
    public function getStripeCustomerId()
    {
        return $this->stripeCustomerId;
    }

    /**
     * @param $stripeCustomerId
     * @return User
     */
    public function setStripeCustomerId($stripeCustomerId): User
    {
        $this->stripeCustomerId = $stripeCustomerId;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     *
     * @return User
     */
    public function setId($id): User
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getSubscriptions()
    {
        return $this->subscriptions;
    }

    /**
     * @param mixed $subscriptions
     *
     * @return User
     */
    public function setSubscriptions($subscriptions)
    {
        $this->subscriptions = $subscriptions;
        return $this;
    }



    /**
     * Returns the roles granted to the user.
     *
     * <code>
     * public function getRoles()
     * {
     *     return array('ROLE_USER');
     * }
     * </code>
     *
     * Alternatively, the roles might be stored on a ``roles`` property,
     * and populated in any number of different ways when the user object
     * is created.
     *
     * @return array (Role|string)[] The user roles
     */
    public function getRoles():array
    {
        return array_unique($this->roles);
    }

    /**
     * Returns the password used to authenticate the user.
     *
     * This should be the encoded password. On authentication, a plain-text
     * password will be salted, encoded, and then compared to this value.
     *
     * @return string The password
     */
    public function getPassword()
    {
        return null;
    }

    /**
     * Returns the salt that was originally used to encode the password.
     *
     * This can return null if the password was not encoded using a salt.
     *
     * @return string|null The salt
     */
    public function getSalt()
    {
        return null;
    }

    /**
     * Removes sensitive data from the user.
     *
     * This is important if, at any given point, sensitive information like
     * the plain-text password is stored on this object.
     */
    public function eraseCredentials()
    {
        return true;
    }

    public function addRoles(array $roles) {
        foreach ($roles as $role) {
            $this->roles[] = $role;
        }
        return $this;
    }

    /**
     * @return mixed
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * @param mixed $username
     *
     * @return User
     */
    public function setUsername($username): User
    {
        $this->username = $username;
        return $this;
    }

    /**
     * Specify data which should be serialized to JSON
     * @link https://php.net/manual/en/jsonserializable.jsonserialize.php
     * @return mixed data which can be serialized by <b>json_encode</b>,
     * which is a value of any type other than a resource.
     * @since 5.4.0
     */
    public function jsonSerialize()
    {
        return [
            "id" => $this->getId(),
            "email" => $this->getEmail(),
            "picture_url" => $this->getPicture(),
            "roles" => $this->getRoles(),
            "company_id" => $this->getCompanyId(),
            "given_name" => $this->getGivenName(),
            "services" => $this->getSubscriptions()
        ];
    }
}