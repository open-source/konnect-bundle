<?php


namespace Aboutgoods\KonnectBundle\EventSubscriber;


use Symfony\Component\HttpKernel\Event\RequestEvent;

class LocaleFromBrowserEventSubscriber
{
    public function onKernelRequest($event)
    {
        if ($event instanceof RequestEvent) {
            $request = $event->getRequest();
            $request->setLocale($request->getPreferredLanguage(["en", "fr"]));
            $request->setDefaultLocale($request->getLocale());
        }
    }

}