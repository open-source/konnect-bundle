Konnect Bundle
====================

This project aims to mutualize the code used by konnect and its modules.
Konnect Bundle have three main goals:
- Give the project a way to authenticate using Auth0
    - All the common Auth0 files are stored here to avoid duplicating them in all project
    - Allow Requests to Konnect API and get informations about user and service.
Take a look at [the Konnect Bundle API documentation](https://doc.agoods.fr/v2/api/konnectBundle.html)
    - Centralize errors templates in order to have same errors pages on each Kweeri Modules.
    
Konnect Bundle import some classes in your module:
- Auth0ResourceOwner that make call to Auth0 for the Authentication
- User entity to have a common User across all our modules
- User Provider to populate the User Entity with Auth0 response

## Installation
You need to install this package with composer using the following command:

```sh
composer req aboutgoods/konnect-bundle
```

**Symfony < 4**   
You need to add:             
```
new Aboutgoods\KonnectBundle\KonnectBundle(),
```
in your AppKernel file (`app/AppKernel.php`).

**Symfony 4+**  
You need to add:             
```
Aboutgoods\KonnectBundle\KonnectBundle::class => ['all' => true],
```
in `/config/bundles.php`

 
## Configuration

##### If you need to make request to the API of Konnect you need to configure Konnect-bundle

In your `konnect.yml`:
```yaml
# Default configuration for "KonnectBundle"
konnect:
  authentication:
    token: '%env(KONNECT_TOKEN)%' #token of your services on konnect
    secret: '%env(KONNECT_SECRET)%' #secret of your services on konnect
    url: '%env(KONNECT_URI)%'
  imgproxy:
    key: '%env(IMGPROXY_KEY)%'
    salt: '%env(IMGPROXY_SALT)%'
    url: '%env(IMGPROXY_URI)%'
```

In your `.env`:
```dotenv
KONNECT_TOKEN=YOUR_SERVICE_TOKEN
KONNECT_SECRET=YOUR_SERVICE_SECRET
KONNECT_URI=127.0.0.1:10080

IMGPROXY_KEY=41626F7574676F6F6473 #Aboutgoods
IMGPROXY_SALT=436F6D70616E79 #Company
IMGPROXY_URI=https://images.agkit.io/
```

For the basic services you can find their `TOKEN` and `SECRET` [on the Konnect readme](https://gitlab.agoods.fr/presentation/kweeri/Kweeri-Konnect/blob/dev/README.md#token-and-secret-for-each-services-).

And for how we implemented img-proxy, you can read [our documentation about image storage](https://gitlab.agoods.fr/documentation/dev-documentation/tree/master/Storage/image-storage).

You can also have a look to the [img-proxy documentation](https://docs.imgproxy.net/#/).

##### If you need to use error templates, you need to add this configuration :

In your `twig.yaml` :
```yaml
twig:
    paths:
        '%kernel.project_dir%/templates': DefaultTemplates
        '%kernel.root_dir%/../vendor/aboutgoods/konnect-bundle/Resources/templates': KonnectBundle
    debug: '%kernel.debug%'
    strict_variables: '%kernel.debug%'
    exception_controller: Aboutgoods\KonnectBundle\Controller\CustomExceptionController::showAction
```

In your `services.yaml` :
```yaml
  Aboutgoods\KonnectBundle\Controller\CustomExceptionController:
    public: true
    arguments:
      $debug: '%kernel.debug%'
```

## Usage

Once this library is installed and configured you need to import all the routes of this bundle:

In your `annotations.yaml` you need to add :

```yaml
konnect-bundle:
  resource: "@KonnectBundle/Controller/"
  type: annotation
  prefix: /konnect
```
After that all the routes are imported you need to use the user provider in your `security.yml`:
```yaml
security:
  providers:
    hwi:
      id: Aboutgoods\KonnectBundle\Security\Provider\UserProvider
  firewalls:
    main:
      provider: hwi
      anonymous: ~
      oauth:
        resource_owners:
          auth0: "/auth0/callback"
        login_path: /login
        use_forward: false
        failure_path: /login

        oauth_user_provider:
          service: Aboutgoods\KonnectBundle\Security\Provider\UserProvider
      logout:
        path:   /auth0/logout
        target: /
        success_handler: App\Listener\LogoutListener
```
After this you should connect to your application and call the routes in this bundle to call Konnect.
You can find RAML documentation [on dev-documentation.](https://doc.agoods.fr/v2/api/konnectBundle.html).

## Permissions and voter
Voters has been added to the project : 
Just use them as classic voters. Available voter roles : 
```
CSV_EXPORT => Useful for checking if the feature CSV Export is enabled
PROMOTIONS => Check if can manage campaigns
QC => Check if plan contains Quality Check
RECEIPT => Basic platform Access
SHERLOCK => Check if anomaly detection should be shown
```
Same as previous voters, but with quota not at 0 check
```
CSV_EXPORT.QUOTA
PROMOTIONS.QUOTA
QC.QUOTA
RECEIPT.QUOTA
SHERLOCK.QUOTA
```

You can also filter by level : 
```
ADMIN (role for aboutgoods)
CLIENT (common client role)
READ_ONLY (role for client who failed to pay)
```

For example, before calling campaigns list, you can add at the top of the controller:
```php
$this->denyUnlessGranted("PROMOTION");
```
Or if we add quota to export 
```php
if(!$this->isGranted("CSV_EXPORT.QUOTA")){
// redirect to plans/refill page
}
```

More information about how to use voters on [symfony documentation](https://symfony.com/doc/current/security/voters.html#setup-checking-for-access-in-a-controller).

### img-proxy

The route `/konnect/resource` is available to serve resources from our own img-proxy server (uri set with the `IMGPROXY_URI` env key).
It accepts some query parameters to configure the way we are serving the resource :
- `url` : the url of the resource
- `resize` : (default `'fit'`) The resize parameter of the image. Can be 'fit', 'fill', or 'auto'.
See [the img-proxy documentation about resize](https://docs.imgproxy.net/#/generating_the_url_advanced?id=resizing-type)
- `width` : (default `'0'`, the original width) The width of the image, in pixels
- `height` : (default `'0'`, the original height) The height of the image, in pixels
- `gravity` : (default `'ce'`) When img-proxy needs to cut some parts of the image, it is guided by the gravity.
See [the img-proxy documentation about gravity](https://docs.imgproxy.net/#/generating_the_url_basic?id=gravity)
- `enlarge` : (default `1`) When set to 1, t or true, img-proxy will enlarge the image if it is smaller than the given size.
See [the img-proxy documentation about enlarge](https://docs.imgproxy.net/#/generating_the_url_basic?id=enlarge)
- `useDefaultExtension` : (default `false`) Boolean to determine whether we should reformat the image (`false`) or use the default extension of the resource (`true`)

## Use this library in an empty Symfony project


You can find [a detailed documentation here](https://gitlab.agoods.fr/documentation/dev-documentation/blob/master/Support/Konnect/HowToCreateAKonnectModule.md).

## Development FAQ


##### You need to add a route to Konnect-Bundle ?

- Set a route on a Controller
> If you add a new controller, you need to declare this controller in `Resources/config/services.yml` with the same syntax that existent controllers

##### You need to add a file to use it on modules ?

- Put your files on a folder
- Use it on the module by specifying the Konnect-Bundle namespace

